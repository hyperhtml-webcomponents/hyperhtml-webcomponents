var fs = require("fs");
var fileContent = "";
[
    "./node_modules/document-register-element/build/document-register-element.js",
    "./node_modules/@webcomponents/shadydom/shadydom.min.js",
    "./node_modules/@webcomponents/shadycss/custom-style-interface.min.js",
    "./node_modules/@webcomponents/shadycss/apply-shim.min.js",
    "./node_modules/@webcomponents/shadycss/scoping-shim.min.js",
].forEach(file => {

    var currentFileContent = fs.readFileSync(file, "utf8");
    fileContent = fileContent + currentFileContent;

});
if (!fs.existsSync("./dist")) {
    fs.mkdirSync("./dist");
}
fileContent = fileContent.replace(new RegExp(' sourceMappingURL=', 'g'), ' ');


fs.writeFileSync("./dist/polyfills.js", fileContent, "utf8");
