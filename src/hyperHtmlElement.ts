import { html, svg, render } from "../node_modules/uhtml/esm";

const UpdateOnConnected = Symbol("UPDATE_ON_CONNECTED");

declare const HTMLElement: {
  prototype: HTMLElement;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  new (arg?: any): HTMLElement;
};

export abstract class HyperHTMLElement extends HTMLElement {
  static html = html;

  static svg = svg;

  static render = render;

  private styleElement;

  private shadow: ShadowRoot;

  private _enableUpdatingResolver: (() => void) | undefined;

  private _updatePromise: Promise<void>;

  constructor(...arg) {
    super(...arg);
    this._updatePromise = new Promise(
      (res) => (this._enableUpdatingResolver = res)
    );
    const { observedProperties = [], useShadowRoot = false } = this
      .constructor as any;
    this[UpdateOnConnected] = [];
    if (useShadowRoot) {
      this.shadow = this.attachShadow({ mode: "open" });
    }
    observedProperties.forEach((propName) => {
      const initialValue = this[propName];
      const CACHED_VALUE = Symbol(propName);
      this[CACHED_VALUE] = initialValue;
      Object.defineProperty(this, propName, {
        get() {
          return this[CACHED_VALUE];
        },
        set(value) {
          const oldValue = this[CACHED_VALUE];
          this[CACHED_VALUE] = value;
          this.dispatchEvent(
            new CustomEvent("propertyChange", {
              detail: {
                oldValue,
                property: propName,
                value,
              },
            })
          );
          if (this.propertyChangedCallback) {
            this.propertyChangedCallback(propName, oldValue, value);
          }
          this.render();
        },
      });
      if (typeof initialValue !== "undefined") {
        this[UpdateOnConnected].push(propName);
      }
    });
  }

  public get updateComplete() {
    return this._getUpdateComplete();
  }

  protected _getUpdateComplete() {
    return this._updatePromise;
  }

  public render() {
    let renderDone = false;
    if ((this as any).template) {
      render(this.shadow ? this.shadow : this, (this as any).template);
      renderDone = true;
    }
    if (renderDone && this.styleElement) {
      if (this.shadowRoot.firstElementChild?.tagName === "LINK") {
        if((this.shadowRoot.firstElementChild as HTMLLinkElement).href.indexOf("blob:") === 0){
          URL.revokeObjectURL((this.shadowRoot.firstElementChild as HTMLLinkElement).href);
        }
        this.shadowRoot.removeChild(this.shadowRoot.firstElementChild);
      }
      this.shadowRoot.insertBefore(
        this.styleElement,
        this.shadowRoot.firstChild
      );
      this.styleElement = false;
    }
    if (renderDone && this._enableUpdatingResolver !== undefined) {
      this._enableUpdatingResolver();
      this._enableUpdatingResolver = undefined;
      this._updatePromise = new Promise(
        (res) => (this._enableUpdatingResolver = res)
      );
    }
  }

  public get html() {
    return HyperHTMLElement.html;
  }

  public get svg() {
    return HyperHTMLElement.svg;
  }

  public initStyle(css: string) {
    return new Promise((resolve /* reject */) => {
      if (this.shadowRoot) {
        const doStyling = () => {
          const checkShadyCSS = () => {
            const template = document.createElement("template");
            template.innerHTML = `<style>${css}<style>`;
            const ShadyCSS = (window as any).ShadyCSS;
            if (typeof ShadyCSS.prepareTemplate === "function") {
              ShadyCSS.prepareTemplate(template, this.tagName.toLowerCase());
              const templateStyle = document
                .importNode(template.content, true)
                .querySelector("style");

              if (ShadyCSS.styleElement) {
                ShadyCSS.styleElement(this);
              }
              if (ShadyCSS.styleSubtree) {
                ShadyCSS.styleSubtree(this);
              }
              if (ShadyCSS.styleDocument) {
                ShadyCSS.styleDocument();
              }
              if ((this.shadowRoot as any).adoptedStyleSheets) {
                const styleSheet = new CSSStyleSheet();
                (styleSheet as any).replaceSync(templateStyle.textContent);
                (this.shadowRoot as any).adoptedStyleSheets = [styleSheet];
              } else {
                const linkElement = document.createElement(
                  "link"
                ) as HTMLLinkElement;
                const blob = new Blob([templateStyle.textContent], {
                  type: "text/css",
                });
                linkElement.href = URL.createObjectURL(blob);
                linkElement.rel = "stylesheet";
                this.styleElement = linkElement;
              }
              resolve(true);
            } else {
              setTimeout(checkShadyCSS, 1);
            }
          };
          checkShadyCSS();
          this.render();

          if (
            this.shadowRoot.firstElementChild?.tagName === "STYLE" &&
            this.shadowRoot.firstElementChild.nextElementSibling.tagName ===
              "STYLE"
          ) {
            this.shadowRoot.removeChild(
              this.shadowRoot.firstElementChild.nextElementSibling
            );
          }
        };
        setTimeout(doStyling, 50);
        window.addEventListener("customStyleConnected", () => {
          setTimeout(doStyling, 50);
        });
      }
    });
  }
}
