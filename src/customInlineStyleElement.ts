declare const HTMLStyleElement: {
  prototype: HTMLStyleElement;
  new(arg?: any): HTMLStyleElement;
};

export class CustomInlineStyleElement extends HTMLStyleElement {

  constructor(...arg) {
    super(...arg);
    if (!this.hasAttribute("data-id")) {
      const s = document.createElement("style");
      const id = "CustomStyle" + Math.floor(Math.random() * 999999) + 1;
      s.textContent = this.textContent;
      s.setAttribute("id", id);
      this.setAttribute("data-id", id);
      document.getElementsByTagName("head")[0].appendChild(s);
      (window as any).ShadyCSS.CustomStyleInterface.addCustomStyle(s);
      (window as any).ShadyCSS.styleSubtree(this);
      this.dispatchEvent(new CustomEvent("customStyleConnected", {
        bubbles: true,
        cancelable: false,
        detail: {
          customStyle: s,
        },
      }));
    }
  }

}
