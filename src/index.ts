

import { HyperHTMLElement } from "./hyperHtmlElement";
import { CustomLinkElement } from "./customLinkElement";
import { CustomInlineStyleElement } from "./customInlineStyleElement";

import installCustomElements from "document-register-element/pony"

declare global {
  interface Window {
    HyperHTMLElement?: typeof HyperHTMLElement;
 }
}

const isNative = (fn) => {
    return (/\{\s*\[native code\]\s*\}/).test("" + fn);
};

if (!isNative((document as any).registerElement)) {
    installCustomElements(window, "auto");
}

window.HyperHTMLElement = HyperHTMLElement;

export default HyperHTMLElement;

customElements.define("custom-style", CustomLinkElement, { extends: "link" });

customElements.define("custom-inline-style", CustomInlineStyleElement, {extends: "style"});
