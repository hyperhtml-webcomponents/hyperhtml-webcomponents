const css = ".foo{color: red;";

class HUIRadioButtonElement extends HyperHTMLElement {
  private static observedAttributes = [];

  private static useShadowRoot = true;

  foo = "foo"

  bar = "bar";

  static observedProperties = ["foo", "bar"]

  public connectedCallback() {
    this.initStyle(css);
    this.render();
  }

  private get template() {
    return this.html`
    <div>Foo ${this.foo} ${this.bar}</div>`;
  }

}

customElements.define("hui-sample", HUIRadioButtonElement);
