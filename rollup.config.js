import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import babel from 'rollup-plugin-babel';
import pkg from './package.json';
import { terser } from "rollup-plugin-terser";
import serve from 'rollup-plugin-serve';

const extensions = ['.js', '.jsx', '.ts', '.tsx'];

const name = 'HyperHTMLElement';

const plugins = [

  // minify
  terser(),

  // Allows node_modules resolution
  resolve({ extensions }),

  // Allow bundling cjs modules. Rollup doesn't understand cjs
  commonjs(),

  // Compile TypeScript/JavaScript files
  babel({ extensions, include: ['src/**/*'] }),

  // serve({
  //   contentBase: ['./', 'dist', 'demo'],
  //   port: 9000,
  //   host: "0.0.0.0"
  // })


];

// serve('./'),

export default [{
  input: './src/index.ts',

  // Specify here external modules which you don't want to
  // include in your bundle (for instance: 'lodash', 'moment' etc.)
  // https://rollupjs.org/guide/en#external-e-external
  external: ["uhtml"],

  plugins,

  output: [
    {
      file: pkg.main,
      format: 'cjs',
    },
    {
      file: pkg.module,
      format: 'es',
    },
    {
      file: pkg.browser,
      format: 'iife',
      name,
      // https://rollupjs.org/guide/en#output-globals-g-globals
      globals: {},
    }],

},
{
  input: './src/sample.ts',

  plugins,

  output: [
    {
      file: "sample.cjs.js",
      format: 'cjs',
    },
    {
      file: "sample.esm.js",
      format: 'es',
    },
    {
      file: "sample.js",
      format: 'iife',
      name: "sample",
      // https://rollupjs.org/guide/en#output-globals-g-globals
      globals: {},
    }],

}];
