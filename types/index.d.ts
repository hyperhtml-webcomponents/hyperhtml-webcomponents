declare const HTMLElement: {
  prototype: HTMLElement;
  new (arg?: any): HTMLElement;
};

// todo better typings for static uhtml fn
declare global {
  abstract class HyperHTMLElement extends HTMLElement {
    static html: any;
    static svg: any;
    static render: (n?: any, r?: any) => any;
    private styleElement;
    private shadow;
    constructor(...arg: any[]);
    render(): void;
    get html(): any;
    get svg(): any;
    get updateComplete(): Promise<void>;
    initStyle(css: string): Promise<unknown>;
  }
}

export default HyperHTMLElement;
